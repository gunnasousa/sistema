# Sistema de empréstimo e agendamento de equipamentos escolar
### O sistema serve para agendar os equipamentos e espaços da escola através de uma página web para uma melhor organização dos laboratórios.

## Pré-requisitos 
* * *
* É essencial um navegador web (exemplo google chrome) e também de um aplicativo que efetue a instalação automática de um conjunto de softwares no computador, de modo a facilitar a configuração de um software interpretador de scripts local e um banco de dados no sistema Windows, exemplo: wampserver e xampp;
* 1024 MB RAM (recomendado);
* 5 GB livres em disco ;

## Funcionalidades
***
* Páginas permitem o CRUD de equipamentos (Datashow, caixa de som,
notebook, ou qualquer outro equipamento que a escola venha a ter);
* Páginas que permitem o CRUD de usuários (professores, etc. (quem vai poder
agendar os equipamentos));
* Páginas que permitem o CRUD de agendamento de laboratórios (informática,
enfermagem, biologia, química, física, matemática, etc.);
* Cadastro de futuro equipamentos (que ainda não existem), bem como
de laboratórios;
* Os   usuários   podem   agendar   os   equipamentos   através   da  página   web   (desde   que
autenticados). O próprio sistema  verifica a disponibilidade do que se está agendando;
* Os administradores do laboratório podem verificar e acompanhar os
agendamentos. O administrador tem a prerrogativa de cancelar um agendamento. Nesse caso o
usuário que agendou receberá uma notificação;
* Os   administradores   do   laboratório  podem   dar   baixa   quando   receberem   os   equipamentos
emprestados;
* O sistema notifica o administrador do laboratório quando um equipamento não for
devolvido;
* O sistema permite fazer um inventário dos equipamentos (relatórios).
 

## Instalando
* * *
* WAMPP v3.2.1

1-No navegador da internet, vá para
  http://www.apachefriends.org/en/xampp-windows.html ;

2-Clique no link para download do XAMPP ;

3-Assim que terminar o download, instale o programa 
  e clique em "Executar" ;

4-Aceite as configurações padrão. Um comando abrirá e
  oferecerá uma linha de instalação inicial. Apenas 
  pressione a tecla “Enter” e aceitar as configurações
  padrão. Para simplificar a instalação é só apertar 
  ”Enter” quando solicitado na linha de comando. Você 
  sempre poderá mudar as configurações editando os 
  arquivos de configuração mais tarde ;

5-Quando terminar a instalação, saia da janela de comando
  digitando “x” na linha de comando ;

6-Inicie o Painel de Controle do XAMPP ;

7-nicie os componentes Apache e MySQL .

* MySQL Workbench 

1-No inicio da instalação teremos três opções, sendo que uma
  delas é para instalação, vamos seguir adiante clicando em 
  INSTALL MYSQL PRODUCTS ;

2-Clique em I ACCEPT THE LICENSE TERMS e clique em NEXT ;

3-Agora teremos duas opções,que são Conectar a internet 
  e Buscar informações de atualização de produto.Estas duas 
  opções servem para o momento da instalação, o próprio 
  instalador do MySQL verifica se há versões mais recentes do
  produto, caso não ache necessário esta opção, simplesmente 
  marque SKIP THE CHECK FOR UPDATES ;

4-Neste próximo passo teremos várias opções, neste caso, isso 
  vai depender de cada profissional e para que ele vai usar o 
  MySQL. Note que no lado esquerdo o aplicativo nos dá informações
  bem detalhadas do que será instalado. Após a sua escolha, marque 
  a opção de sua escolha e clique em NEXT ;

5-Neste próximo passo existe um componente detectado que não está 
  instalado no seu computador(Microsoft Visual C++ 2010 32-bit runtime
  MySQL Workbench CE 5.2.42),mas não há problema, clique em EXECUTE que
  o próprio assistente vai baixá-lo para você ;

6-Após este processo, clique em NEXT para dar continuidade, a próxima 
  janela mostrará tudo que será instalado ;

7-Após este passo clique em EXECUTE. Após o termino deste processo, o botão
  NEXT ficará habilitado, clique nele e continue até a próxima janela 
  CONFIGURATION OVERVIEW (visão geral da configuração) ;

8-Clique em NEXT para dar continuidade ao processo. Na configuração do MySQL,
  podemos mudar a porta utilizada e o tipo de configuração do servidor: 
  Developer Machine, Server Machine e Dedicated Machine, para o nosso caso, 
  vamos deixar do jeito que está, clique em NEXT para irmos ao próximo passo.
  Após clicar em NEXT, aparecerá uma janela para inserir uma senha para o administrador ;

9-Após definir a senha do administrador clique em NEXT. Na próxima janela tem o WINDOWS SERVICE NAME,
  a opção de inicializar o MySQL com o sistema operacional e por último executar como conta do sistema
  padrão ou usuário personalizado. Caso deseje, desmarque a opção de inicializar o MySQL, já as outras 
  configurações, deixe como está, clique em NEXT para o próximo passo ;

10-Apos o término, clique em NEXT para seguir para o próximo passo. Após este processo aparecerá uma 
   janela informando a finalização do processo de instalação do MySQL, clique em finish e pronto.

11-Agora vamos visualizar o novo MySQL. Clique no botão INICIAR, TODOS OS PROGRAMAS, MYSQL,
   MYSQL WORKBENCH 5.2.CE ;

12-Para entrarmos na ferramenta, clique duas vezes em LOCALINSTANCE MYSQL55, em seguida aparecerá
   uma pequena tela com login e senha, entre com a senha informada na instalação e clique em OK, 
   pronto, finalmente veremos um novo visual do MySQL ;

13-Com isso finalizamos a instalação desse SGBD que agora está pronto para uso.

* Composer

1- Opções do instalador
--install-dir
  Você pode instalar o composer em um diretório específico usando a opção --install-dir e 
fornecendo um diretório de destino.
  Exemplo: php composer-setup.php --install-dir = bin
--nome do arquivo
  Você pode especificar o nome do arquivo (default: composer.phar) usando a opção --filename.
  Exemplo: php composer-setup.php --filename = compositor
--versão
  Você pode instalar o composer em um release específico usando a opção --version e fornecendo um release de destino.
  Exemplo: php composer-setup.php --version = 1.0.0-alpha8

## Executando os testes
* * *
### Resultado dos testes em breve!

## Construído com
* * *
* Visual studio versão 1.9.1- Aplicativo para criação e gerenciamento de códigos;
* Workbench versão 5.1.16- Para desenvolvimento do banco de dados;
* XAMPP Control Panel Versão 3.2.2 - Para ativar o  phpMyAdmin e o apache.
* Apache versão 2.4.25 - Usado como servidor web;
* phpMyAdmin versão 4.6.5.2 - Ultilizado para adicionar e adiministrar bancos de dados;
* PHP versão 7.1.1 - Versão da linguagem;
* Composer versão 1.6.3 - Gerenciador de dependências;
* Git versão 2.17 - Como sistema de controle de versão de arquivos.


## Autores
* * *
* José Eduardo - Trabalho inicial;
* Gunnavingren de Sousa - Trabalho inicial;
* Gessé Mariano - Trabalho inicial;
* Raul Martins -Trabalho inicial;
* Junvenildo Vaz - Tecnólogo e orientador.
 


 
